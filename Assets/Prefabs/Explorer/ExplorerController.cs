﻿using UnityEngine;


public class ExplorerController : MonoBehaviour
{
    [SerializeField] private float speedMultiplier;
    [SerializeField] private float rotationMultiplier;
    [SerializeField] private Transform cameraTransform;

    private Vector3 _velocity = Vector3.zero;
    private Vector2 _angVelocity = Vector3.zero;
    private Vector2 _viewAngles;

    private Transform _transform;
    private Rigidbody _rb;
    
    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        
        _transform = GetComponent<Transform>();
        _rb = GetComponent<Rigidbody>();

        // get starting angles
        _viewAngles = new Vector2(_transform.rotation.eulerAngles.y, cameraTransform.rotation.eulerAngles.x);
    }

    private void Update()
    {
        _velocity = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical")) * speedMultiplier;
        _angVelocity = new Vector2(Input.GetAxis("Mouse X"), -Input.GetAxis("Mouse Y"));
    }

    private void FixedUpdate()
    {
        // movement
        var nextPoint = _transform.TransformPoint(cameraTransform.localRotation * _velocity);
        _rb.velocity = Vector3.zero;
        _rb.MovePosition(nextPoint);

        // looking around
        _viewAngles += _angVelocity * (rotationMultiplier * Time.fixedDeltaTime);

        _rb.MoveRotation(Quaternion.Euler(0, _viewAngles.x, 0));
        cameraTransform.localRotation = Quaternion.Euler(_viewAngles.y, 0, 0);
    }
}