﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(LevelGenerator))]
public class LevelGeneratorDrawer : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (GUILayout.Button("Generate Level"))
        {
            var generator = (LevelGenerator)target;
            generator.Generate();
        }
        
        if (GUILayout.Button("Run updates without re-init"))
        {
            var generator = (LevelGenerator)target;
            generator.Generate(false);
        }
    }
}