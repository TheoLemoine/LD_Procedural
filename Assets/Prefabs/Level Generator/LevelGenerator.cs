﻿using Unity.Collections;
using UnityEditor;
using UnityEngine;
using UnityEngine.Rendering;

[CreateAssetMenu(fileName = "Generator", menuName = "ScriptableObjects/LevelGenerator", order = 1)]
public class LevelGenerator : ScriptableObject
{
    [SerializeField] private CustomRenderTexture texture;
    [SerializeField] private RT2Tex3DConvertor convertor;
    public float seed = 1;
    public int nbIterations = 50;
    public string outputPath = "Generated/output";

    public void Generate(bool fromStart = true)
    {
        if(fromStart) texture.Initialize();
        
        texture.initializationMaterial.SetFloat("_Seed", seed);
        
        texture.EnsureDoubleBufferConsistency();
        texture.Update(nbIterations);
        
        if (texture.dimension == TextureDimension.Tex3D)
        {
            convertor.renderTexture = texture;
            convertor.outputPath = outputPath;
            convertor.Save();
        }
        else if (texture.dimension == TextureDimension.Tex2D)
        {
            var output = new Texture2D(texture.width, texture.height, TextureFormat.R8, false);
            RenderTexture.active = texture;
            output.ReadPixels(new Rect(0, 0, texture.width, texture.height), 0, 0);
            output.Apply();

            AssetDatabase.CreateAsset(output, "Assets/" + outputPath + ".asset");
        }
    }
    
}