﻿using UnityEditor;
using UnityEngine;

/**
 * extracting 3D texture from render texture is hard
 * code mainly from https://answers.unity.com/questions/840983/how-do-i-copy-a-3d-rendertexture-isvolume-true-to.html?childToView=1243556#answer-1243556
 * Thanks to Nesvi
 */
[CreateAssetMenu(fileName = "Convertor", menuName = "ScriptableObjects/RenderTexture to Texture 3D Convertor", order = 1)]
public class RT2Tex3DConvertor : ScriptableObject
{
    [SerializeField] private ComputeShader slicer;
    [HideInInspector] public RenderTexture renderTexture;
    [HideInInspector] public string outputPath = "Generated/output";
    
    RenderTexture Copy3DSliceToRenderTexture(RenderTexture source, int layer)
    {
        RenderTexture render = new RenderTexture(source.width, source.height, 0, source.format);
        render.dimension = UnityEngine.Rendering.TextureDimension.Tex2D;
        render.enableRandomWrite = true;
        render.wrapMode = TextureWrapMode.Clamp;
        render.Create();

        int kernelIndex = slicer.FindKernel("CSMain");
        slicer.SetTexture(kernelIndex, "voxels", source);
        slicer.SetInt("layer", layer);
        slicer.SetTexture(kernelIndex, "Result", render);
        slicer.Dispatch(kernelIndex, source.width, source.height, 1);

        return render;
    }

    Texture2D ConvertFromRenderTexture(RenderTexture rt)
    {
        Texture2D output = new Texture2D(rt.width, rt.height, TextureFormat.R8, true);
        RenderTexture.active = rt;
        output.ReadPixels(new Rect(0, 0, rt.width, rt.height), 0, 0);
        output.Apply();
        return output;
    }

    public void Save()
    {
        RenderTexture[] layers = new RenderTexture[renderTexture.volumeDepth];
        for (int i = 0; i < renderTexture.volumeDepth; i++)
        {
            layers[i] = Copy3DSliceToRenderTexture(renderTexture, i);
        }

        Texture2D[] finalSlices = new Texture2D[renderTexture.volumeDepth];
        for (int i = 0; i < renderTexture.volumeDepth; i++)
        {
            finalSlices[i] = ConvertFromRenderTexture(layers[i]);
        }

        Texture3D output = new Texture3D(renderTexture.width, renderTexture.height, renderTexture.volumeDepth, TextureFormat.R8, true);
        output.filterMode = FilterMode.Trilinear;
        Color[] outputPixels = output.GetPixels();

        for (int k = 0; k < renderTexture.volumeDepth; k++) 
        { 
            Color[] layerPixels = finalSlices[k].GetPixels();
            for (int i = 0; i < renderTexture.width; i++)
            {
                for (int j = 0; j < renderTexture.height; j++)
                {
                    outputPixels[i + j * renderTexture.height + k * renderTexture.width * renderTexture.height] = layerPixels[i + j * renderTexture.height];
                }
            }
        }

        output.SetPixels(outputPixels);
        output.Apply();

        AssetDatabase.CreateAsset(output, "Assets/" + outputPath + ".asset");
    }
}
