Shader "CustomRenderTexture/Init/All Random 2D"
{
    Properties 
    {
        _Seed("Seed", Float) = 452.245
    }

    SubShader
    {
        Lighting Off
        Blend One Zero

        Pass
        {
            CGPROGRAM
            #include "UnityCustomRenderTexture.cginc"

            #pragma vertex InitCustomRenderTextureVertexShader
            #pragma fragment frag
            #pragma target 3.0

            float _Seed;

            float random (float2 uv)
            {
                return frac(sin(dot(uv,float2(12.9898,78.233))+_Seed)*43758.5453123);
            }

            float4 frag(v2f_init_customrendertexture IN) : COLOR
            {
                return random(IN.texcoord);
            }
            ENDCG
        }
    }
}