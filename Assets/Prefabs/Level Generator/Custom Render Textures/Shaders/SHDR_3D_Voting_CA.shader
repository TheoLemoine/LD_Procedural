Shader "CustomRenderTexture/Voting Cellular Automata 3D"
{
    Properties
    {
        _OnMinValue("ON Min value", Range(0, 1)) = 0.5
        _Majority("Majority", Range(0, 1)) = 0.5
    }

    SubShader
    {
        Lighting Off
        Blend One Zero

        Pass
        {
            CGPROGRAM
            #include "UnityCustomRenderTexture.cginc"
            #pragma vertex CustomRenderTextureVertexShader
            #pragma fragment frag
            #pragma target 3.0

            float _OnMinValue;
            float _Majority;

            static const int nb_neighbours_directions = 26;
            static const float3 neighbours_directions[] = {
                // cross around point
                float3(1, 0, 0),
                float3(0, 1, 0),
                float3(0, 0, 1),
                float3(-1, 0, 0),
                float3(0, -1, 0),
                float3(0, 0, -1),

                // diags
                float3(1, 1, 0),
                float3(1, -1, 0),
                float3(-1, -1, 0),
                float3(-1, 1, 0),
                
                float3(0, 1, 1),
                float3(0, 1, -1),
                float3(0, -1, -1),
                float3(0, -1, 1),
                
                float3(1, 0, 1),
                float3(1, 0, -1),
                float3(-1, 0, -1),
                float3(-1, 0, 1),

                // corners
                float3(1, 1, 1), // up
                float3(-1, 1, 1),
                float3(-1, 1, -1),
                float3(1, 1, -1),
                
                float3(1, -1, 1), // down
                float3(-1, -1, 1),
                float3(-1, -1, -1),
                float3(1, -1, -1),
            };

            float4 frag(v2f_customrendertexture IN) : COLOR
            {
                const float3 uv_step_vector = 1 / _CustomRenderTextureInfo.xyz;

                int nb_on = tex3D(_SelfTexture3D, IN.globalTexcoord).r > _OnMinValue ? 1 : 0;

                for (int i = 0; i < nb_neighbours_directions; i++)
                {
                    float3 neighbours_uv = IN.globalTexcoord + uv_step_vector * neighbours_directions[i];
                    float vote = tex3D(_SelfTexture3D, neighbours_uv).r;

                    if(vote > _OnMinValue) nb_on++;
                }
                
                return nb_on >= nb_neighbours_directions * _Majority ? float4(1, 1, 1, 1) : float4(0, 0, 0, 0);
            }
            ENDCG
        }
    }
}