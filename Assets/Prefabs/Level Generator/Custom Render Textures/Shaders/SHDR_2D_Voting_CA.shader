Shader "CustomRenderTexture/Voting Cellular Automata 2D"
{
    Properties
    { 
        _OnMinValue("ON Min value", Range(0, 1)) = 0.5
        _Majority("Majority", Range(0, 1)) = 0.5
    }

    SubShader
    {
        Lighting Off
        Blend One Zero

        Pass
        {
            CGPROGRAM
            #include "UnityCustomRenderTexture.cginc"
            #pragma vertex CustomRenderTextureVertexShader
            #pragma fragment frag
            #pragma target 3.0

            float _OnMinValue;
            float _Majority;

            static const int nb_neighbours_directions = 14;
            static const float2 neighbours_directions[] =  {
                // circle around
                float2(1, 0),
                float2(1, 1),
                float2(0, 1),
                float2(-1, 1),
                float2(-1, 0),
                //float2(-1, -1),
                float2(0, -1),
                float2(1, -1),

                // extended circle
                float2(2, 0),
                float2(0, 2),
                //float2(-2, 0),
                //float2(0, -2),

                // flow direction maybe ?
                float2(1, 2),
                float2(2, 2),
                float2(2, 1),
                float2(-1, 2),
                float2(2, -1),
            };

            float4 frag(v2f_customrendertexture IN) : COLOR
            {
                const float2 uv_step_vector = 1 / _CustomRenderTextureInfo.xy;

                // start with you own vote
                int nb_on = tex2D(_SelfTexture2D, IN.globalTexcoord.xy).r > _OnMinValue ? 1 : 0;

                // add votes next of neighbours
                for (int i = 0; i < nb_neighbours_directions; i++)
                {
                    float2 neighbours_uv = IN.globalTexcoord.xy + uv_step_vector * neighbours_directions[i];
                    float vote = tex2D(_SelfTexture2D, neighbours_uv).r;

                    if(vote > _OnMinValue) nb_on++;
                }

                // get winner
                return nb_on > nb_neighbours_directions * _Majority ? float4(1, 1, 1, 1) : float4(0, 0, 0, 0);
            }
            ENDCG
        }
    }
}