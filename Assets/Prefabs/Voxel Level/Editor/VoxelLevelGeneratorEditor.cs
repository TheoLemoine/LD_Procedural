﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(VoxelLevelGenerator))]
public class VoxelLevelGeneratorEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        
        if (GUILayout.Button("Regenerate level"))
        {
            var generator = (VoxelLevelGenerator)target;
            generator.RunCellAutomata();
        }
        
        if (GUILayout.Button("One more step"))
        {
            var generator = (VoxelLevelGenerator)target;
            generator.OneMoreStep();
        }
        
        if (GUILayout.Button("Bake occlusion and light"))
        {
            var generator = (VoxelLevelGenerator)target;
            generator.BakeOcclusionAndLight();
        }
    }
}