﻿using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.Experimental.Rendering;
using UnityEngine.Serialization;

public class VoxelLevelGenerator : MonoBehaviour
{
    [Header("Cellular Automata")]
    [SerializeField] private LevelGenerator generator;
    [SerializeField] private float seed;
    [SerializeField] private int nbIterations;
    
    [Header("Voxels Generator")]
    [SerializeField] private GameObject voxelPrefab;
    [SerializeField] private Vector3 voxelDimensions;
    [SerializeField] private Texture3D texture;

    [SerializeField] private Material TopMaterial;
    [SerializeField] private Material DownMaterial;

    private static Vector3Int[] _facingDirections =
    {
        new Vector3Int(1, 0, 0),
        new Vector3Int(0, 1, 0),
        new Vector3Int(0, 0, 1),
        new Vector3Int(-1, 0, 0),
        new Vector3Int(0, -1, 0),
        new Vector3Int(0, 0, -1),
    };
    
    public void Generate()
    {
        for (int i = transform.childCount; i > 0; --i)
            DestroyImmediate(transform.GetChild(0).gameObject);

        var halfDimensions = new Vector3(texture.width, texture.height, texture.depth) / 2;
        halfDimensions.Scale(voxelDimensions);
        
        for (int x = 0; x < texture.width; x++)
        {
            for (int y = 0; y < texture.height; y++)
            {
                for (int z = 0; z < texture.depth; z++)
                {
                    if (IsSolid(x, y, z))
                    {
                        if (_facingDirections.All(dir => IsInBounds(x + dir.x, y + dir.y, z + dir.z) && IsSolid(x + dir.x, y + dir.y, z + dir.z)))
                        {
                            // skip voxel if it is covered in every direction
                            continue;
                        }

                        var voxel = Instantiate(voxelPrefab, transform);
                        var pos = voxelDimensions;
                        pos.Scale(new Vector3(x, y, z));
                        voxel.transform.localPosition = pos - halfDimensions;
                        
                        // check if is on top
                        voxel.GetComponent<Renderer>().material = IsInBounds(x, y + 1, z) && IsSolid(x, y + 1, z) ? DownMaterial : TopMaterial;
                    }
                }
            }
        }
    }

    private bool IsSolid(int x, int y, int z) 
        => texture.GetPixel(x, y, z).r > 0.5;

    private bool IsInBounds(int x, int y, int z) 
        => x >= 0 && y >= 0 && z >= 0 && x < texture.width && y < texture.height && z < texture.depth;

    public void RunCellAutomata()
    {
        generator.seed = seed;
        generator.nbIterations = nbIterations;
        generator.Generate();
        texture = AssetDatabase.LoadAssetAtPath<Texture3D>("Assets/" + generator.outputPath + ".asset");
        Generate();
    }

    public void OneMoreStep()
    {
        generator.seed = seed;
        generator.nbIterations = 1;
        generator.Generate(false);
        texture = AssetDatabase.LoadAssetAtPath<Texture3D>("Assets/" + generator.outputPath + ".asset");
        Generate();
    }

    public void BakeOcclusionAndLight()
    {
        StaticOcclusionCulling.Compute();
        Lightmapping.Bake();
    }
}